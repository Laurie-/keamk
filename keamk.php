<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="keamk.css">
    <title>Document</title>
</head>
<body class="bg-dark">
    <div class="container ">
        <div class="row main">
    <?php

$promo = ['Esma ', 'Esther ', 'Fatima ', 'Florence ', 'Grace ', 'Hayette ', 'Isabelle ', 'Laurie ', 'Léa ', 'Marion ', 'Maude ', 'Nathalie ', 'Otilia ', 'Sixtine ', 'Syriane ', 'Viviane '];

    // $nb;
    $long = sizeof($promo); // longueur du tableau
    $num = 0;
    for ($j=1; $j <=4 ; $j++) { 
        
        $num = $num+1;
        echo "<div class='table col-md-5 bg-secondary'>";
        echo "<h1>Groupe $num</h1>";
        echo "<div class = row>";
        for ($i=1; $i <=4 ; $i++) { 
            $long = sizeof($promo);  //renvoi la longueur du tableau
            $nb = rand(0, $long-1);  // renvoi un nombre aléatoire entre 0 et la longueur du tableau -1
            // echo $nb;
            echo "<div class='nom col-md-5'>$promo[$nb]</div>"; // Affiche une valeur aléatoire (nb) du tableau
            unset($promo[$nb]); // Supprime la valeur aléatoire du tableau 
            $promo = array_values($promo); // Réindex le tableau 
            // echo $promo[rand(0, 15)];
            // print_r($promo);
        }
        echo"</div>";
        echo"</div>";
        echo"<br/>";
        echo"<br/>";
    }
?>
        </div>
    </div>
    
</body>
</html>

